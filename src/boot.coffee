express = require 'express'
load = require 'express-load'
Router = require 'named-routes'
bodyParser = require 'body-parser'
mongoose = require 'mongoose'

app = express()
env = app.get('env') or 'development'
router = new Router()

app.use bodyParser.json()

router.extendExpress app
router.registerAppHelpers app

load 'config'
  .into app.locals

load 'models', cwd: 'app'
  .then 'controllers', cwd: 'app'
  .then 'helpers', cwd: 'app'
  .then 'routes', cwd: 'app'
  .into app

load 'lib'
  .into global

mongourl = app.locals.config.mongodb[env].url or process.env.MONGOLAB_URI
mongoose.connect mongourl,
  server:
    socketOptions:
      keepAlive: 1

mongoose.connection.on 'error', (err)->
  console.log "Mongoose default connection error: #{err}"
  return
mongoose.connection.on 'disconnected', ->
  console.log "Mongoose default connection disconnected"
  return

# If the Node process ends, close the Mongoose connection
process.on 'SIGINT', ->
  mongoose.connection.close ->
    console.log 'Mongoose default disconnected through app termination'
    process.exit 0
    return

exports.app = app
exports.port = process.env.PORT or 3000

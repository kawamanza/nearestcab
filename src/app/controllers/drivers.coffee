_ = require 'underscore'
mongoose = require 'mongoose'
DriverStatus = mongoose.model 'driver_status'
Driver = mongoose.model 'driver'

verifyDriverInfo = (body) ->
  if _.isEmpty body
    message: 'empty body'
  else
    lib.verify body
      .check 'name', 'present required'
      .check 'carPlate', 'present required carPlate'
      .report 'vnd.error'

verifyGeolocationParams = (body) ->
  if _.isEmpty body
    message: 'empty body'
  else
    lib.verify body
      .check 'latitude', 'present nonNull decimal'
      .check 'longitude', 'present nonNull decimal'
      .check 'driverId', 'present nonNull integer'
      .check 'driverAvailable', 'present nonNull boolean'
      .report 'vnd.error'

projectionStatus =
  latitude: 1
  longitude: 1
  driverId: 1
  driverAvailable: 1

pickStatus = (driverStatus) ->
  _.pick(driverStatus, _.keys(projectionStatus)...)

saveDriverStatus = (attributes, callback) ->
  statusAttributes = _.omit(pickStatus(attributes), 'driverId')
  statusAttributes.loc = [attributes.longitude, attributes.latitude]
  DriverStatus.findOneAndUpdate
    driverId: attributes.driverId
  , statusAttributes
  , upsert: true
  , callback
  return

projectionDriver =
  driverId: 1
  name: 1
  carPlate: 1

saveDriver = (attributes, callback) ->
  lib.nextSequence 'driverId', (id) ->
    if id
      attrs = _.pick(attributes, _.keys(projectionDriver)...)
      attrs.driverId = id
      driver = new Driver attrs
      driver.save (err, model, numberAffected) ->
        callback err, model
        return
    else
      callback true, null
    return
  return

loadDriversInArea = (req, callback) ->
  errors = lib.verify req.query
    .check 'sw', 'geolocation'
    .check 'ne', 'geolocation'
    .report 'default'
  unless _.isEmpty errors
    callback errors, []
    return
  ne = _.map req.query.ne.split(','), (v) -> Number.parseFloat(v)
  sw = _.map req.query.sw.split(','), (v) -> Number.parseFloat(v)
  # http://docs.mongodb.org/manual/reference/operator/query/box/#op._S_box
  query =
    loc:
      $geoWithin:
        $box: [
          [sw[1], sw[0]]
          [ne[1], ne[0]]
        ]
    driverAvailable: true
  delete query.driverAvailable if req.query.all is "true"
  DriverStatus.find(query, callback)
  return

loadDriversNear = (req, callback) ->
  errors = lib.verify req.query
    .check 'geo', 'geolocation'
    .check 'dist', 'onlyDigits'
    .report 'default'
  unless _.isEmpty errors
    callback errors, []
    return
  geo = _.map req.query.geo.split(','), (v) -> Number.parseFloat(v)
  # http://docs.mongodb.org/manual/reference/operator/query/minDistance/#op._S_minDistance
  query =
    loc:
      $near:
        $geometry:
          type: "Point"
          coordinates: [geo[1], geo[0]]
        $minDistance: 0
        $maxDistance: parseInt req.query.dist, 10
    driverAvailable: true
  delete query.driverAvailable if req.query.all is "true"
  DriverStatus.find(query, callback)
  return

# GET /drivers/:id/status
exports.geolocation = (req, res) ->
  driverId = parseInt req.params.id, 10
  DriverStatus.findOne driverId: driverId, projectionStatus, (err, driver) ->
    if err
      console.log 'DriverStatus.findOne error', err
      res.status(500).type('text/plain').send('error fetching driver status')
    else if driver is null
      res.status(404).type('text/plain').send('')
    else
      res.status(200).json(pickStatus driver)
    return
  return

# POST /drivers/:id/status
exports.newGeolocation = (req, res) ->
  if _.isObject req.body
    errors = verifyGeolocationParams req.body
    if _.isEmpty(errors) and req.body.driverId.toString() isnt req.params.id
      errors =
        _embedded:
          errors: [
            path: "/driverId"
            message: "must match pathParam"
          ]
    unless _.isEmpty errors
      res.status(422).type('application/vnd.error+json').json errors
    else
      saveDriverStatus req.body, (err, model) ->
        if err
          res.status(422).json message: 'unknown error'
        else
          res.status(201).type('text/plain').send('')
        return
  else
    res.status(422).json
      error:
        message: 'empty body'
  return

# POST /drivers
exports.create = (req, res) ->
  if _.isObject req.body
    errors = verifyDriverInfo req.body
    unless _.isEmpty errors
      res.status(422).type('application/vnd.error+json').json errors
    else
      saveDriver req.body, (err, model) ->
        if err
          res.status(422).json message: 'unknown error'
        else if model
          res.setHeader 'Location', "/drivers/#{model.driverId}"
          res.status(201).type('text/plain').send('')
        else
          res.status(500).json message: 'unknown error'
        return
  else
    res.status(422).json
      error:
        message: 'empty body'
  return

# GET /drivers/:id
exports.show = (req, res) ->
  driverId = parseInt req.params.id, 10
  Driver.findOne
    driverId: driverId
  , projectionDriver
  , (err, driver) ->
    if err
      res.status(500).json message: 'unknown error'
    else if driver
      driver = _.pick driver, _.keys(projectionDriver)...
      res.status(200).json(driver)
    else
      res.status(404).type('text/plain').send('')
    return
  return

# GET /drivers/inArea?sw=lat,long&ne=lat,long
exports.inArea = (req, res) ->
  if _.isEmpty req.query
    res.setHeader('Warning', 'Query parameters required (sw+ne)')
    res.status(400).send('')
    return
  errors = lib.verify req.query
    .check 'sw', 'required'
    .check 'ne', 'required'
    .report 'default'
  if errors
    res.setHeader('Warning', "Query parameters missing (#{_.keys errors})")
    res.status(400).send('')
    return
  loadDriversInArea req, (err, list) ->
    if err
      res.setHeader('Warning', 'Error loading drivers')
      res.status(422).send('')
    else
      list = _.map list, pickStatus
      res.status(200).json(list)
    return
  return

# GET /drivers/near?geo=lat,long&dist=meters
exports.near = (req, res) ->
  if _.isEmpty req.query
    res.setHeader('Warning', 'Query parameters required (geo+dist)')
    res.status(400).send('')
    return
  errors = lib.verify req.query
    .check 'geo', 'required'
    .check 'dist', 'required'
    .report 'default'
  if errors
    res.setHeader('Warning', "Query parameters missing (#{_.keys errors})")
    res.status(400).send('')
    return
  loadDriversNear req, (err, list) ->
    if err
      res.setHeader('Warning', 'Error loading drivers')
      res.status(422).send('')
    else
      list = _.map list, pickStatus
      res.status(200).json(list)
    return
  return

exports.root = (req, res) ->
  res.setHeader 'Cache-Control', "max-age=#{3 * 24 * 60 * 60}, public"
  res.redirect 301, req.app.namedRoutes.build 'entrypoint_path'
  return

exports.entrypoint = (req, res) ->
  body =
    _links:
      self:
        href: req.app.namedRoutes.build 'entrypoint_path'
      driver:
        href: req.app.namedRoutes.build 'driver_path', id: '{id}'
        templated: true
      driver_status:
        href: req.app.namedRoutes.build 'driver_status_path', id: '{id}'
        templated: true
      drivers_inArea:
        href: "#{req.app.namedRoutes.build 'drivers_area_path'}{?sw,ne}"
        templated: true
      drivers_near:
        href: "#{req.app.namedRoutes.build 'drivers_near_path'}{?geo,dist}"
        templated: true

  res.setHeader 'Cache-Control', "max-age=#{10 * 60}, public"
  res.json body
  return

mongoose = require 'mongoose'


Driver = mongoose.Schema
  driverId:
    type: Number
    required: true
  name:
    type: String
    required: true
  carPlate:
    type: String
    required: true


mongoose.model 'driver', Driver

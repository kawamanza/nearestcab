mongoose = require 'mongoose'


DriverStatus = mongoose.Schema
  latitude:
    type: Number
    required: true
  longitude:
    type: Number
    required: true
  loc:
    type: [Number]
    required: true
    index: '2d'
  driverId:
    type: Number
    required: true
  driverAvailable:
    type: Boolean


mongoose.model 'driver_status', DriverStatus

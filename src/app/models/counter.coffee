mongoose = require 'mongoose'


Counter = mongoose.Schema
  name:
    type: String
    required: true
  seq:
    type: Number
    required: true


mongoose.model 'counter', Counter

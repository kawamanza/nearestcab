module.exports = (app) ->
  drivers = app.controllers.drivers
  app.get '/drivers/inArea', 'drivers_area_path', drivers.inArea
  app.get '/drivers/near', 'drivers_near_path', drivers.near
  app.get '/drivers/:id/status', 'driver_status_path', drivers.geolocation
  app.post '/drivers/:id/status', drivers.newGeolocation
  app.post '/drivers', drivers.create
  app.get '/drivers/:id', 'driver_path', drivers.show
  return

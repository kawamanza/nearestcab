module.exports = (app) ->
  app.get '/', 'root_path', app.controllers.welcome.root
  app.get '/entrypoint', 'entrypoint_path', app.controllers.welcome.entrypoint
  return

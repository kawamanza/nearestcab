chai = require 'chai'
expect = chai.expect
boot = require '../../../boot.js'

describe 'lib.verify', ->
  it 'should validate required', ->
    object =
      name: ""
    errors = lib.verify object
      .check 'name', 'required'
      .report 'default'
    expect(errors).to.deep.eq name: "required"
    return
  it 'should validate geolocation', ->
    object =
      gps: "20.1,-30.1"
    errors = lib.verify object
      .check 'gps', 'geolocation'
      .report 'default'
    expect(errors).to.eq undefined
    return
  return

request = require 'supertest'
boot = require '../../../boot.js'
chai = require 'chai'
expect = chai.expect
_ = require 'underscore'
mongoose = require 'mongoose'

mongoose.connection.db.dropCollection 'counters', (err, result) ->
  console.log 'db.counters :: cleanig error:', err if err
  return
mongoose.connection.db.dropCollection 'drivers', (err, result) ->
  console.log 'db.drivers :: cleanig error:', err if err
  return

describe 'OPTIONS /drivers/:id/status', ->
  it 'should return header Options', (done) ->
    request boot.app
      .options '/drivers/1/status'
      .expect 200
      .expect 'Allow', 'GET,HEAD,POST'
      .end done
    return
  return

describe 'POST /drivers', ->
  context 'with invalid data', ->
    it 'should return 422 when no content sent', (done) ->
      request boot.app
        .post '/drivers'
        .set 'Content-Type', 'application/json'
        .expect 422
        .expect 'Content-Type', /vnd\.error\+json/
        .expect /empty body/
        .end done
      return
    it 'should return 422 when attributes error', (done) ->
      request boot.app
        .post '/drivers'
        .set 'Content-Type', 'application/json'
        .send
          name: 'Marcelo'
        .expect 422
        .expect 'Content-Type', /vnd\.error\+json/
        .expect /\{"path":"\/carPlate","message":"[^"]+be present"\}/
        .end done
      return
    it 'should return 422 when invalidattributes', (done) ->
      request boot.app
        .post '/drivers'
        .set 'Content-Type', 'application/json'
        .send
          name: 'Marcelo'
          carPlate: 'aaa-1234'
        .expect 422
        .expect 'Content-Type', /vnd\.error\+json/
        .expect /\{"path":"\/carPlate","message":"[^"]+valid car plate/
        .end done
      return
    return
  context 'with valid data', ->
    it 'should return 201', (done) ->
      request boot.app
        .post '/drivers'
        .set 'Content-Type', 'application/json'
        .send
          name: 'Marcelo'
          carPlate: 'DXX-9999'
        .expect 201
        .expect 'Content-Type', /text\/plain/
        .expect 'Location', /^\/drivers\/\d+$/
        .end done
      return
    return
  return

describe 'GET /drivers/:id', ->
  it 'should return 200', (done) ->
    request boot.app
      .get '/drivers/1'
      .expect 200
      .expect 'Content-Type', /application\/json/
      .expect (res) ->
        expect(res.body).to.be.a 'object'
        expect(res.body).to.have.all.keys [
          'driverId'
          'name'
          'carPlate'
        ]
        return
      .end done
    return
  it 'should return 404 for unknown driver', (done) ->
    request boot.app
      .get '/drivers/8776'
      .expect 404
      .expect 'Content-Type', /text\/plain/
      .end done
    return
  return

describe 'POST /drivers/:id/status', ->
  context 'with invalid data', ->
    it 'should return 422 when no content sent', (done) ->
      request boot.app
        .post '/drivers/1/status'
        .expect 422
        .expect 'Content-Type', /vnd\.error\+json/
        .expect /empty body/
        .end done
      return
    it 'should return 422 when attributes error', (done) ->
      request boot.app
        .post '/drivers/1/status'
        .set 'Content-Type', 'application/json'
        .send
          latitude: "-23.612474"
          longitude: -46.702746
          # driverId: 1
          driverAvailable: ""
        .expect 422
        .expect 'Content-Type', /vnd\.error\+json/
        .expect /\{"path":"\/latitude","message":"[^"]+decimal number"\}/
        .expect /\{"path":"\/driverId","message":"must be present"\}/
        .expect /\{"path":"\/driverAvailable","message":"[^"]+true or false"\}/
        .end done
      return
    it 'should return 422 when driverId does not match pathParam', (done) ->
      request boot.app
        .post '/drivers/1/status'
        .set 'Content-Type', 'application/json'
        .send
          latitude: -23.612474
          longitude: -46.702746
          driverId: 2
          driverAvailable: true
        .expect 422
        .expect 'Content-Type', /vnd\.error\+json/
        .expect /\{"path":"\/driverId","message":"must match pathParam"\}/
        .end done
      return
    return
  context 'with valid data', ->
    it 'should return 201', (done) ->
      request boot.app
        .post '/drivers/1/status'
        .set 'Content-Type', 'application/json'
        .send
          latitude: -23.612474
          longitude: -46.702746
          driverId: 1
          driverAvailable: true
        .expect 201
        .expect 'Content-Type', /text\/plain/
        .end done
      return
    return
  return

describe 'GET /drivers/:id/status', ->
  it 'should return 200', (done) ->
    request boot.app
      .get '/drivers/1/status'
      .expect 200
      .expect 'Content-Type', /application\/json/
      .expect (res) ->
        expect(res.body).to.be.a 'object'
        expect(_.keys(res.body)).to.deep.equal [
          'latitude'
          'longitude'
          'driverId'
          'driverAvailable'
        ]
        expect(res.body.latitude).to.be.a 'number'
        expect(res.body.longitude).to.be.a 'number'
        return
      .end done
    return
  it 'should return 404 for unknown driverStatus', (done) ->
    request boot.app
      .get '/drivers/8776/status'
      .expect 404
      .expect 'Content-Type', /text\/plain/
      .end done
    return
  return

describe 'GET /drivers/inArea{?sw,ne}', ->
  context 'invalid query params', ->
    it 'should return 400 for empty query params', (done) ->
      request boot.app
        .get '/drivers/inArea'
        .expect 400
        .expect 'Warning', 'Query parameters required (sw+ne)'
        .end done
      return
    it 'should return 400 for missing query params pair', (done) ->
      request boot.app
        .get '/drivers/inArea'
        .query
          sw: "-23.612474,-46.702746"
        .expect 400
        .expect 'Warning', 'Query parameters missing (ne)'
        .end done
      return
    return
  context 'valid query params', ->
    it 'should return 200', (done) ->
      request boot.app
        .get '/drivers/inArea'
        .query
          sw: "-23.612474,-46.702746"
          ne: "-23.589548,-46.673392"
        .expect 200
        .expect 'Content-Type', /application\/json/
        .expect (res) ->
          expect(res.body).to.have.length(1)
          expect(res.body[0]).to.be.a 'object'
          expect(res.body[0]).to.have.all.keys [
            'latitude'
            'longitude'
            'driverId'
            'driverAvailable'
          ]
          return
        .end done
      return
    return
  return

describe 'GET /drivers/near{?geo,dist}', ->
  context 'invalid query params', ->
    it 'should return 400 for empty query params', (done) ->
      request boot.app
        .get '/drivers/near'
        .expect 400
        .expect 'Warning', 'Query parameters required (geo+dist)'
        .end done
      return
    it 'should return 400 for missing query params pair', (done) ->
      request boot.app
        .get '/drivers/near'
        .query
          geo: "-23.612474,-46.702746"
        .expect 400
        .expect 'Warning', 'Query parameters missing (dist)'
        .end done
      return
    return
  context 'valid query params', ->
    it 'should return 200', (done) ->
      request boot.app
        .get '/drivers/near'
        .query
          geo: "-23.612474,-46.702746"
          dist: "500"
        .expect 200
        .expect 'Content-Type', /application\/json/
        .expect (res) ->
          expect(res.body).to.have.length(1)
          expect(res.body[0]).to.be.a 'object'
          expect(res.body[0]).to.have.all.keys [
            'latitude'
            'longitude'
            'driverId'
            'driverAvailable'
          ]
          return
        .end done
      return
    return
  return

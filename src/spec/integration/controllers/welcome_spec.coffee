request = require 'supertest'
boot = require '../../../boot.js'
expect = require('chai').expect

describe 'GET /', ->
  it 'should return 301', (done) ->
    request boot.app
      .get '/'
      .expect 301, /Moved Permanently/
      .expect 'Location', '/entrypoint'
      .expect 'Cache-Control', 'max-age=259200, public'
      .end done
    return
  return

describe 'GET /entrypoint', ->
  it 'should return JSON', (done) ->
    request boot.app
      .get '/entrypoint'
      .expect 200
      .expect 'Cache-Control', 'max-age=600, public'
      .expect (res) ->
        expect(res.body).to.be.a 'object'
        links = res.body._links
        expect(links).to.be.a 'object'
        expect(links).to.have.all.keys [
          'self'
          'driver'
          'drivers_inArea'
          'drivers_near'
          'driver_status'
        ]
        expect(links.driver.href).to.match /drivers\/\{id\}/
        expect(links.driver_status.href).to.match /\/\{id\}\/status/
        return
      .end done
    return
  return

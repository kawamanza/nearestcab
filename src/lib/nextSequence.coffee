module.exports = ->
  mongoose = require 'mongoose'

  # http://docs.mongodb.org/manual/tutorial/create-an-auto-incrementing-field/
  nextSequence = (name, callback) ->
    Counter = mongoose.model 'counter'
    Counter.findOneAndUpdate
      name: name
    , $inc: { seq: 1 }
    ,
      upsert: true
      new: true
    , (err, model) ->
      if err or not model
        console.log 'Error getting sequence', err, 'model', model
        callback 0
      else if model
        callback model.seq
      return
    return

  nextSequence
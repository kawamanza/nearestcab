module.exports = ->
  class VndErrorReport
    build: (stack) ->
      return unless stack.length
      errors = []
      for failResult in stack
        errors.push
          path: '/'.concat failResult.path.join('/')
          message: failResult.message or steps[failResult.step].message

      _embedded:
        errors: errors
  class SimpleErrorReport
    build: (stack) ->
      return unless stack.length
      errors = {}
      for failResult in stack
        errors[failResult.path.join('.')] = steps[failResult.step].message
      errors


  formats =
    "vnd.error": new VndErrorReport()
    "default": new SimpleErrorReport()

  steps =
    present:
      check: (obj, field) -> obj.hasOwnProperty field
      message: "must be present"
    nonNull:
      check: (obj, field) -> obj[field] isnt null
      message: "must be non-null"
    required:
      check: (obj, field) -> obj[field] and true
      message: "required"
    decimal:
      check: (obj, field) -> n = obj[field]; Number(n) is n
      message: "must be a valid decimal number"
    integer:
      check: (obj, field) -> n = obj[field]; Number(n) is n and n % 1 is 0
      message: "must be a valid number"
    onlyDigits:
      check: (obj, field) -> /^\d+$/.test obj[field]
      message: 'must have only digits'
    boolean:
      check: (obj, field) -> b = obj[field]; Boolean(b) is b
      message: "must be true or false"
    geolocation:
      check: (obj, field) -> /^-?\d+\.\d+,-?\d+\.\d+$/.test obj[field]
      message: "must be a valid geolocation (lat,long)"
    carPlate:
      check: (obj, field) -> /^[A-Z]{3}-\d{4}$/.test obj[field]
      message: "must be a valid car plate (AAA-dddd)"


  class Fail
    constructor: (@step, @path) ->

  class Verification
    constructor: (@_obj, @_path = [], @_stack = []) ->
    check: (field, pipeline) ->
      pipeline = pipeline.split /\s/ if typeof pipeline is 'string'
      for step in pipeline
        throw new Error "Invalid step: #{step}" unless steps[step]
        unless steps[step].check @_obj, field
          @_stack.push new Fail step, @_path.concat field
          break
      @
    report: (format) ->
      formats[format].build(@_stack)

  verify = (obj) -> new Verification obj
  verify.reportFormats = formats

  return verify

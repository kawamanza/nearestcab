Instruções para teste e execução
================================

O projeto está escrito em NodeJS, então a primeira coisa a se fazer é
instalar os pacotes necessários (nodejs+npm).
Siga as instruções no link:

https://github.com/joyent/node/wiki/installing-node.js-via-package-manager

Testes automatizados
--------------------

Os testes geram resultados na pasta ./coverage

```
npm test
```

Ativar o serviço
----------------

```
npm start
```


module.exports = (grunt) ->

  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'

    coffee:
      options:
        bare: true
      compile:
        expand: true
        cwd: 'src'
        src: ['**/*.coffee']
        dest: '.'
        ext: '.js'
    coffeelint:
      app: 'src/**/*.coffee'
    watch:
      files: ['Gruntfile.coffee', 'src/**/*.coffee']
      tasks: ['build']


  grunt.loadNpmTasks 'grunt-coffeelint'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'


  grunt.registerTask 'build', ['coffeelint', 'coffee']
  grunt.registerTask 'default', ['build', 'watch']


  return
